# zabbix monitor debian updates

Esse projeto foi desenvolvido para disponibilizar uma forma facil de monitorar quando servidores Debian possuem atualizações disponiveis, utilizando o pacote cron-apt

Mais detalhes sobre esse projeto é só acessar a postagem que fiz no meu site https://www.adonai.eti.br/2023/02/monitorando-pelo-zabbix-atualizacoes-do-debian/