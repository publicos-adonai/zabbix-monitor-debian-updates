#!/bin/bash

apt-get update
apt-get install cron-apt
echo 'SYSLOGON="always"' >> /etc/cron-apt/config
echo 'RUNSLEEP=3600' >> /etc/cron-apt/config
echo 'DEBUG="always"' >> /etc/cron-apt/config
adduser zabbix adm
systemctl restart zabbix-agent2.service
